quietly set ACTELLIBNAME SmartFusion2
quietly set PROJECT_DIR "/home/konrad/Workspace/FPGA/Private/Hello_world/Hello_World"
source "${PROJECT_DIR}/simulation/CM3_compile_bfm.tcl";

if {[file exists presynth/_info]} {
   echo "INFO: Simulation library presynth already exists"
} else {
   file delete -force presynth 
   vlib presynth
}
vmap presynth presynth
vmap SmartFusion2 "/home/konrad/Tools/Libero/Libero_v12.2/Libero/lib/modelsimpro/precompiled/vlog/smartfusion2"

vcom -2008 -explicit  -work presynth "${PROJECT_DIR}/component/work/FCCC_C1/FCCC_C1_0/FCCC_C1_FCCC_C1_0_FCCC.vhd"
vcom -2008 -explicit  -work presynth "${PROJECT_DIR}/component/work/FCCC_C1/FCCC_C1.vhd"
vcom -2008 -explicit  -work presynth "${PROJECT_DIR}/component/work/Hello_World_MSS/Hello_World_MSS.vhd"
vcom -2008 -explicit  -work presynth "${PROJECT_DIR}/hdl/led_switches.vhd"
vcom -2008 -explicit  -work presynth "${PROJECT_DIR}/component/work/Hello_World/Hello_World.vhd"

vsim -L SmartFusion2 -L presynth  -t 1fs presynth.Hello_World
# The following lines are commented because no testbench is associated with the project
# add wave /testbench/*
# run 1000ns
