# Netlist Viewer TCL File
set_family -name SmartFusion2
top_module -name Hello_World
addfile -view RTL -lib work -type VHDL -mode vhdl_2008 -name {/home/konrad/Workspace/FPGA/Private/Hello_world/Hello_World/component/work/Hello_World_MSS/Hello_World_MSS_syn.vhd}
addfile -view RTL -lib work -type VHDL -mode vhdl_2008 -name {/home/konrad/Workspace/FPGA/Private/Hello_world/Hello_World/component/work/Hello_World_MSS/Hello_World_MSS.vhd}
addfile -view RTL -lib work -type VHDL -mode vhdl_2008 -name {/home/konrad/Workspace/FPGA/Private/Hello_world/Hello_World/hdl/led_switches.vhd}
addfile -view RTL -lib work -type VHDL -mode vhdl_2008 -name {/home/konrad/Workspace/FPGA/Private/Hello_world/Hello_World/component/work/Hello_World/Hello_World.vhd}
addfile -view HIER -lib work -type VLOG -mode system_verilog -name {/home/konrad/Workspace/FPGA/Private/Hello_world/Hello_World/synthesis/Hello_World.vm}
addfile -view FLAT -lib work -type AFL -mode NONE -name {/home/konrad/Workspace/FPGA/Private/Hello_world/Hello_World/designer/Hello_World/COMPILE/Hello_World.afl}