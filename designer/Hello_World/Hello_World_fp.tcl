new_project \
         -name {Hello_World} \
         -location {/home/konrad/Workspace/FPGA/Private/Hello_world/Hello_World/designer/Hello_World/Hello_World_fp} \
         -mode {chain} \
         -connect_programmers {FALSE}
add_actel_device \
         -device {M2S010} \
         -name {M2S010}
enable_device \
         -name {M2S010} \
         -enable {TRUE}
save_project
close_project
