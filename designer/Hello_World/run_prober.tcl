probe \
    -desdir {/home/konrad/Workspace/FPGA/Private/Hello_world/Hello_World/designer/Hello_World} \
    -design Hello_World \
    -fam SmartFusion2 \
    -die PA4M1000_N \
    -pkg tq144 \
    -use_mvn_pdc 0  \
    -use_last_placement 0 