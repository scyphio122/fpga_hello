--------------------------------------------------------------------------------
-- Company: <Name>
--
-- File: led_switches.vhd
-- File history:
--      <Revision number>: <Date>: <Comments>
--      <Revision number>: <Date>: <Comments>
--      <Revision number>: <Date>: <Comments>
--
-- Description: 
--
-- <Description here>
--
-- Targeted device: <Family::SmartFusion2> <Die::M2S010> <Package::144 TQ>
-- Author: <Name>
--
--------------------------------------------------------------------------------

library IEEE;

use IEEE.std_logic_1164.all;

entity led_switches is
port (
    --<port_name> : <direction> <type>;
	button : IN  std_logic; -- example
    clock : IN std_logic;
    led_0 : OUT std_logic;
    led_1 : OUT std_logic;
    led_2 : OUT std_logic;
    led_3 : OUT std_logic;
    led_4 : OUT std_logic;
    led_5 : OUT std_logic;
    led_6 : OUT std_logic;
    led_7 : OUT std_logic
    --<other_ports>;
);
end led_switches;
architecture architecture_led_switches of led_switches is
   -- signal, component etc. declarations  
    signal timeout  : std_logic := '0';
    signal ticks : integer := 0;
    signal out_sig : std_logic_vector(7 downto 0) := "00000000";
    signal state : integer := 0;
    
begin
    led_0 <= out_sig(0);
    led_1 <= out_sig(1);
    led_2 <= out_sig(2);
    led_3 <= out_sig(3);
    led_4 <= out_sig(4);
    led_5 <= out_sig(5);
    led_6 <= out_sig(6);
    led_7 <= out_sig(7);

    process
    begin
        if button = '1' then
            out_sig(state) <= '1';
            wait on timeout until timeout = '1';
            out_sig(state) <= '0';
            
            state <= state + 1;
            if (state = 8) then
                state <= 0;
            end if;
        else
            out_sig <= (others => '0');
        end if;
    end process;
    
    process(clock)
    begin
        if rising_edge(clock) then
            ticks <= ticks + 1;
            if (ticks = 500000 - 1) then
                timeout <= '1';
                ticks <= 0;
            else
                timeout <= '0';
            end if;
        end if;
    end process;
    
   -- architecture body
end architecture_led_switches;
