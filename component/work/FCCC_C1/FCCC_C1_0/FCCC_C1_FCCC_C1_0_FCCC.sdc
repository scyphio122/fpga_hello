set_component FCCC_C1_FCCC_C1_0_FCCC
# Microsemi Corp.
# Date: 2020-Apr-21 00:27:21
#

create_clock -period 20 [ get_pins { CCC_INST/CLK0_PAD } ]
create_generated_clock -multiply_by 2 -divide_by 2 -source [ get_pins { CCC_INST/CLK0_PAD } ] -phase 0 [ get_pins { CCC_INST/GL0 } ]
