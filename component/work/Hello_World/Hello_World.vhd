----------------------------------------------------------------------
-- Created by SmartDesign Tue Apr 21 00:38:26 2020
-- Version: v12.2 12.700.0.22
----------------------------------------------------------------------

----------------------------------------------------------------------
-- Libraries
----------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;

library smartfusion2;
use smartfusion2.all;
----------------------------------------------------------------------
-- Hello_World entity declaration
----------------------------------------------------------------------
entity Hello_World is
    -- Port list
    port(
        -- Inputs
        BUT_0 : in  std_logic;
        Clock : in  std_logic;
        -- Outputs
        LED_0 : out std_logic;
        LED_1 : out std_logic;
        LED_2 : out std_logic;
        LED_3 : out std_logic;
        LED_4 : out std_logic;
        LED_5 : out std_logic;
        LED_6 : out std_logic;
        LED_7 : out std_logic
        );
end Hello_World;
----------------------------------------------------------------------
-- Hello_World architecture body
----------------------------------------------------------------------
architecture RTL of Hello_World is
----------------------------------------------------------------------
-- Component declarations
----------------------------------------------------------------------
-- FCCC_C1
component FCCC_C1
    -- Port list
    port(
        -- Inputs
        CLK0_PAD : in  std_logic;
        -- Outputs
        GL0      : out std_logic;
        LOCK     : out std_logic
        );
end component;
-- Hello_World_MSS
component Hello_World_MSS
    -- Port list
    port(
        -- Inputs
        MCCC_CLK_BASE : in std_logic
        );
end component;
-- led_switches
component led_switches
    -- Port list
    port(
        -- Inputs
        button : in  std_logic;
        clock  : in  std_logic;
        -- Outputs
        led_0  : out std_logic;
        led_1  : out std_logic;
        led_2  : out std_logic;
        led_3  : out std_logic;
        led_4  : out std_logic;
        led_5  : out std_logic;
        led_6  : out std_logic;
        led_7  : out std_logic
        );
end component;
----------------------------------------------------------------------
-- Signal declarations
----------------------------------------------------------------------
signal FCCC_C1_0_GL0 : std_logic;
signal LED_0_net_0   : std_logic;
signal LED_0_0       : std_logic;
signal LED_2_net_0   : std_logic;
signal LED_3_net_0   : std_logic;
signal LED_4_net_0   : std_logic;
signal LED_5_net_0   : std_logic;
signal LED_6_net_0   : std_logic;
signal LED_7_net_0   : std_logic;
signal LED_0_0_net_0 : std_logic;
signal LED_0_net_1   : std_logic;
signal LED_2_net_1   : std_logic;
signal LED_3_net_1   : std_logic;
signal LED_5_net_1   : std_logic;
signal LED_4_net_1   : std_logic;
signal LED_6_net_1   : std_logic;
signal LED_7_net_1   : std_logic;

begin
----------------------------------------------------------------------
-- Top level output port assignments
----------------------------------------------------------------------
 LED_0_0_net_0 <= LED_0_0;
 LED_0         <= LED_0_0_net_0;
 LED_0_net_1   <= LED_0_net_0;
 LED_1         <= LED_0_net_1;
 LED_2_net_1   <= LED_2_net_0;
 LED_2         <= LED_2_net_1;
 LED_3_net_1   <= LED_3_net_0;
 LED_3         <= LED_3_net_1;
 LED_5_net_1   <= LED_5_net_0;
 LED_5         <= LED_5_net_1;
 LED_4_net_1   <= LED_4_net_0;
 LED_4         <= LED_4_net_1;
 LED_6_net_1   <= LED_6_net_0;
 LED_6         <= LED_6_net_1;
 LED_7_net_1   <= LED_7_net_0;
 LED_7         <= LED_7_net_1;
----------------------------------------------------------------------
-- Component instances
----------------------------------------------------------------------
-- FCCC_C1_0
FCCC_C1_0 : FCCC_C1
    port map( 
        -- Inputs
        CLK0_PAD => Clock,
        -- Outputs
        GL0      => FCCC_C1_0_GL0,
        LOCK     => OPEN 
        );
-- Hello_World_MSS_0
Hello_World_MSS_0 : Hello_World_MSS
    port map( 
        -- Inputs
        MCCC_CLK_BASE => FCCC_C1_0_GL0 
        );
-- led_switches_0
led_switches_0 : led_switches
    port map( 
        -- Inputs
        button => BUT_0,
        clock  => FCCC_C1_0_GL0,
        -- Outputs
        led_0  => LED_0_0,
        led_1  => LED_0_net_0,
        led_2  => LED_2_net_0,
        led_3  => LED_3_net_0,
        led_4  => LED_4_net_0,
        led_5  => LED_5_net_0,
        led_6  => LED_6_net_0,
        led_7  => LED_7_net_0 
        );

end RTL;
